defmodule RemotessWeb.PageController do
  use RemotessWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def login(conn, _params) do
    render(conn, "login.html")
  end

  def sigup(conn, _params) do
    render(conn, "signup.html")
  end

end

defmodule Remotess.Catalog.Note do
  use Ecto.Schema
  import Ecto.Changeset

  schema "notes" do
    field :allow, :string
    field :description, :string
    field :title, :string


    timestamps()
  end

  @doc false
  def changeset(note, attrs) do
    note
    |> cast(attrs, [:title, :description, :allow])
    |> validate_required([:title, :description])
    |> validate_format(:allow, ~r/@/)
    |> validate_length(:title, min: 2)
    |> validate_length(:title, max: 50)
    |> validate_length(:description, min: 1)

  end
end

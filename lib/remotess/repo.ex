defmodule Remotess.Repo do
  use Ecto.Repo,
    otp_app: :remotess,
    adapter: Ecto.Adapters.Postgres
end

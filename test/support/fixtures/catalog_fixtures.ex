defmodule Remotess.CatalogFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Remotess.Catalog` context.
  """

  @doc """
  Generate a note.
  """
  def note_fixture(attrs \\ %{}) do
    {:ok, note} =
      attrs
      |> Enum.into(%{
        allow: "some allow",
        description: "some description",
        title: "some title"
      })
      |> Remotess.Catalog.create_note()

    note
  end
end

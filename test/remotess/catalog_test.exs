defmodule Remotess.CatalogTest do
  use Remotess.DataCase

  alias Remotess.Catalog

  describe "notes" do
    alias Remotess.Catalog.Note

    import Remotess.CatalogFixtures

    @invalid_attrs %{allow: nil, description: nil, title: nil}

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      assert Catalog.list_notes() == [note]
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      assert Catalog.get_note!(note.id) == note
    end

    test "create_note/1 with valid data creates a note" do
      valid_attrs = %{allow: "some allow", description: "some description", title: "some title"}

      assert {:ok, %Note{} = note} = Catalog.create_note(valid_attrs)
      assert note.allow == "some allow"
      assert note.description == "some description"
      assert note.title == "some title"
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalog.create_note(@invalid_attrs)
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      update_attrs = %{allow: "some updated allow", description: "some updated description", title: "some updated title"}

      assert {:ok, %Note{} = note} = Catalog.update_note(note, update_attrs)
      assert note.allow == "some updated allow"
      assert note.description == "some updated description"
      assert note.title == "some updated title"
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalog.update_note(note, @invalid_attrs)
      assert note == Catalog.get_note!(note.id)
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = Catalog.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> Catalog.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = Catalog.change_note(note)
    end
  end
end

# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :remotess,
  ecto_repos: [Remotess.Repo]

# Configures the endpoint
config :remotess, RemotessWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: RemotessWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Remotess.PubSub,
  live_view: [signing_salt: "qq8liXAU"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :remotess, Remotess.Mailer, adapter: Swoosh.Adapters.Local

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.29",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

  config :ueberauth, Ueberauth,
  providers: [
    google: {Ueberauth.Strategy.Google, [send_redirect_uri: false]},
    github: {Ueberauth.Strategy.Github, [send_redirect_uri: false]}
  ]

  config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: {System, :get_env, ["GITHUB_CLIENT_ID"]},
  client_secret: {System, :get_env, ["GITHUB_CLIENT_SECRET"]}

  config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  client_id: {System, :get_env, ["GOOGLE_CLIENT_ID"]},
  client_secret: {System, :get_env, ["GOOGLE_CLIENT_SECRET"]}

  config :tailwind,
  version: "3.1.6",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"

let content = pilih("#content");
function test(val) {
	console.log(val)
}

function keString(node) {
	return node.outerHTML || new XMLSerializer().serializeToString(node)
}

function justifyRight() {
	document.execCommand("justifyRight", false, null)
}
function justifyLeft() {
	document.execCommand("justifyLeft", false, null)
}
function justifyCenter() {
	document.execCommand("justifyCenter", false, null)
}
function insImg() {
	document.execCommand("insertImage", false, "image")
}

tekan("alt e", function() {
	justifyCenter()
})
tekan("alt r", function() {
	justifyRight()
	return false
})